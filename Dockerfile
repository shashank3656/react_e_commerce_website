FROM ubuntu:latest
USER root

RUN apt-get update
RUN apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install nodejs -yq
#RUN npm install

# # Create app directory
RUN mkdir -p /usr/app
WORKDIR /usr/app

# Installing dependencies
COPY ./  /usr/app/
RUN rm -rf package-lock.json node_modules
RUN npm cache clean --force
#RUN npm install node-sass
RUN npm i --unsafe-perm node-sass
RUN npm cache verify
RUN npm install react react-dom react-router-dom react-redux react-scripts react-stripe-checkout redux redux-logger redux-persist reselect
RUN npm install styled-components
RUN npm install react-scripts
RUN npm install nuxt
RUN npm install
RUN npm audit fix
# Copying source files
#COPY ./ /usr/app

EXPOSE 3000
# Building app
#RUN npm run build


# Running the app
#CMD "npm" "run" "dev"

CMD ["npm", "start"]
